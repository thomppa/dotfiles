set nocompatible
filetype off   

call plug#begin()
    


    """""""""""MD/LaTex""""""""""""
    Plug 'lervag/vimtex'
    Plug 'suan/vim-instant-markdown', {'for': 'markdown'} 

    """""""""""Navigation""""""""""
    Plug 'justinmk/vim-sneak'
    Plug 'frazrepo/vim-rainbow'
    Plug 'scrooloose/nerdcommenter'
    Plug 'junegunn/limelight.vim'                 
    Plug 'terryma/vim-multiple-cursors'
    Plug 'ctrlpvim/ctrlp.vim'

    """""""""""""Tagbar""""""""""""
    Plug 'majutsushi/tagbar'
    Plug 'craigemery/vim-autotag'

    
    """"UnitySupport/completion""""
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'omnisharp/omnisharp-vim'
    Plug 'ncm2/float-preview.nvim'
    Plug 'tpope/vim-dispatch'
    Plug 'fatih/vim-go'
    Plug 'octol/vim-cpp-enhanced-highlight'


    Plug 'w0rp/ale'
    Plug 'chiel92/vim-autoformat'

    """""""PythonCompletion"""""""
    Plug 'davidhalter/jedi-vim'
    Plug 'ChristianChiarulli/codi.vim'

    """""""Git/FileManager"""""""""
    Plug 'francoiscabrol/ranger.vim'
    Plug 'tpope/vim-fugitive'

    """""""""""Nerdtree""""""""""""
"    Plug 'scrooloose/nerdtree'                      
"   Plug 'tiagofumo/vim-nerdtree-syntax-highlight' 
    Plug 'ryanoasis/vim-devicons'                 
   
    Plug 'vimwiki/vimwiki'                       
    Plug 'jreybert/vimagit'                     
    
    Plug 'tpope/vim-surround'                  
    
    Plug 'kovetskiy/sxhkd-vim'                        
    Plug 'ap/vim-css-color'                          
    
    Plug 'junegunn/goyo.vim'                        
    
    Plug 'junegunn/vim-emoji'                      

    """"""""""""Themes"""""""""""""
    Plug 'joshdick/onedark.vim'

    Plug 'arcticicestudio/nord-vim'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'

    Plug 'mhinz/vim-startify'


call plug#end()	


filetype plugin indent on   


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General Settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set path+=**					" Searches current directory recursively.
set wildmenu					" Display all matches when tab complete.
set incsearch                   " Incremental search
set hidden                      " Needed to keep multiple buffers open
set nobackup                    " No auto backups
set noswapfile                  " No swap
set t_Co=256                    " Set if term supports 256 colors.
set number relativenumber       " Display line numbers
set clipboard=unnamedplus       " Copy/paste between vim and other programs.
syntax enable
let g:rehash256 = 1


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Remap Keys
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let mapleader = "."

:imap iii <Esc>

map ** :TagbarToggle<CR>
map II :! pdflatex %<CR><CR>
map SS :! killall evince-previewer ; evince-previewer $(echo % \| sed 's/tex$/pdf/') & disown<CR>

map <leader>rr :Ranger<CR>
map <leader>sc :source %<CR>

let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Status Line
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

colorscheme nord 

let g:airline_theme='minimalist'
hi Normal guibg=NONE ctermbg=NONE

let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#tabline#enabled = 1

"let g:lightline = {
      "\ 'colorscheme': 'onedark',
      "\ }

set laststatus=2


set noshowmode



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Codi configs
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:codi#virtual_text_prefix = "=❯ "

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Sneak configs
" TODO: remap labels
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:sneak#label = 1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"OmniSharp
"TODO: Popuphighlight to exceptions, types, def
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:OmniSharp_server_stdio = 1
let OmniSharp_highlight_types = 3
let g:omnicomplete_fetch_full_documentation = 1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set expandtab                   " Use spaces instead of tabs.
set smarttab                    " Be smart using tabs ;)
set shiftwidth=4                " One tab == four spaces.
set tabstop=4                   " One tab == four spaces.

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Theming
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
highlight LineNr           ctermfg=8    ctermbg=none    cterm=none
highlight CursorLineNr     ctermfg=7    ctermbg=8       cterm=none
highlight VertSplit        ctermfg=0    ctermbg=8       cterm=none
highlight Statement        ctermfg=2    ctermbg=none    cterm=none
highlight Directory        ctermfg=4    ctermbg=none    cterm=none
highlight NERDTreeClosable ctermfg=2
highlight NERDTreeOpenable ctermfg=8
highlight Comment          ctermfg=4    ctermbg=none    cterm=none


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VimWiki
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:vimwiki_list = [{'path': '~/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vim-Instant-Markdown
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:instant_markdown_autostart = 1         " Turns on auto preview
let g:instant_markdown_browser = "surf"      " Uses surf for preview

"let g:instant_markdown_slow = 1
  "let g:instant_markdown_open_to_the_world = 1
  "let g:instant_markdown_allow_unsafe_content = 1
  "let g:instant_markdown_allow_external_content = 0
  "let g:instant_markdown_mathjax = 1
  "let g:instant_markdown_logfile = '/tmp/instant_markdown.log'
  "let g:instant_markdown_autoscroll = 0
  "let g:instant_markdown_port = 8888
  "let g:instant_markdown_python = 1

map <Leader>md :InstantMarkdownPreview<CR>   " Previews .md file
map <Leader>ms :InstantMarkdownStop<CR>      " Kills the preview

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Open terminal inside Vim
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <Leader>tt :vnew term://zsh<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Mouse Scrolling
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"set mouse=nicr

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Splits and Tabbed Files
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set splitbelow splitright

" Remap splits navigation to just CTRL + hjkl
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Make adjusing split sizes a bit more friendly
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>

" Change 2 split windows from vert to horiz or horiz to vert
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K

" Removes pipes | that act as seperators on splits
set fillchars+=vert:\ 

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Other Stuff
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:python_highlight_all = 1

au! BufRead,BufWrite,BufWritePost,BufNewFile *.org 
au BufEnter *.org            call org#SetOrgFileType()

set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar


source $HOME/.config/nvim/plug-config/coc.vim

let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1

let g:startify_custom_header = [
            \ '       ______            ______            ______',
            \ '      /\_____\          /\_____\          /\_____\          ____',
            \ '     _\ \__/_/_         \ \__/_/_         \ \__/_/         /\___\',
            \ '    /\_\ \_____\        /\ \_____\        /\ \___\        /\ \___\',
            \ '    \ \ \/ / / /        \ \/ / / /        \ \/ / /        \ \/ / /',
            \ '     \ \/ /\/ /          \/_/\/ /          \/_/_/          \/_/_/',
            \ '      \/_/\/_/              \/_/',
            \ '',
            \ '    New Things to Try out:',
            \ '                     Codi!',
            \ '           CoCMarketplace!',
            \ '                   TagBar!',
            \]
