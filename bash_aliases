### ALIASES ###

# root privileges
alias doas="doas --"
alias sudo='sudo '

# navigation
alias ..='cd ..' 
alias ...='cd ../..'

# vim
alias vim=nvim

# broot
alias br='br -dhp'
alias bs='br --sizes'

alias getdates='date | tee ~/fulldate.txt | cut --delimiter=" " --fields=1 | tee ~/shortdate.txt | xargs echo "Today is"'

alias rmconf='rm -rf config.h'

alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing

alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias lynx='lynx -cfg=~/.lynx/lynx.cfg -lss=~/.lynx/lynx.lss -vikeys'
alias vifm='./.config/vifm/scripts/vifmrun'

# the terminal rickroll
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'

# bare git repo alias for dotfiles
alias config="/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME"

# termbin
alias tb="nc termbin.com 9999"


