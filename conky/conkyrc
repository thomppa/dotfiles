--[[
	Created by Brad Heffernan for Hefftor Edition
]]

conky.config = {
	background = true,
	update_interval = 1,

	cpu_avg_samples = 2,
	net_avg_samples = 2,
	temperature_unit = celsius,

	double_buffer = true,
	no_buffers = true,
	text_buffer_size = 2048,

	gap_x = 0,
	gap_y = 0,

	minimum_width = 680,

	own_window = true,
    own_window_class = 'Conky',
    own_window_type = 'desktop',
    own_window_argb_visual = true,
    own_window_argb_value = 0,
    own_window_hints = 'undecorated,sticky,below,skip_taskbar,skip_pager',
	border_inner_margin = 0,
	border_outer_margin = 0,
	alignment = 'middle_middle',

	draw_shades = true,
	draw_outline = false,
	draw_borders = false,
	draw_graph_borders = false,
	default_bar_width = 280,
	default_bar_height = 3,
	default_outline_color = '000000',

	override_utf8_locale = true,
	use_xft = true,
	font = 'Fivo Sans Thin:size=8',
	xftalpha = 0.8,
	uppercase = false,

	default_color = 'FFFFFF',
	color2 = 'dda9bf',
	color1 = 'FFFFFF',
	color2 = 'dda9bf',
	color3 = '401343',
	color4 = 'fae4df',
	color5 = 'e6d5ec',

	lua_load = '~/dotfiles/conky/lua/day.lua',
	lua_draw_hook_pre = 'conky_day'

}

conky.text = [[
${voffset 250}${goto 240}${font Fivo Sans Heavy:size=14:bold}${texeci 5 sh ~/.lua/mpc_artist}
${goto 240}${font Fivo Sans Heavy:size=10:bold}${texeci 5 sh ~/.lua/mpc_title}
${voffset 10}${color2}${goto 225}${execbar ~/.lua/mpc_progress bar}
${color1}${voffset -4}${goto 470}${texeci 1 sh ~/.lua/mpc_progress length}

${if_running spotify}\
	${voffset -135}
	${image ~/.conky/spoclo/spotify_icon.png -p 130,210 -s 65x65}\
	${goto 220}${voffset 0}${font GE Inspira Bold:size=18}${exec ~/dotfiles/conky/spoclo/scripts/artist.sh}${font}${voffset -0}
	${goto 220}${voffset -12}${font GE Inspira:size=16}${exec ~/dotfiles/conky/spoclo/scripts/title.sh}${font}${voffset 12}\
${endif}\

]];
