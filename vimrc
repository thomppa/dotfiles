call plug#begin()"

Plug 'scrooloose/syntastic'
Plug 'bling/vim-airline'
Plug 'suan/vim-instant-markdown', {'for': 'markdown'}
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-dispatch'
Plug 'omnisharp/omnisharp-vim'
Plug 'kien/ctrlp.vim'
Plug 'lifepillar/vim-mucomplete'
Plug 'joshdick/onedark.vim'
Plug 'cocopon/iceberg.vim'
Plug 'ap/vim-css-color'
Plug 'francoiscabrol/ranger.vim'
Plug 'justinmk/vim-sneak'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-fugitive'

call plug#end()

filetype indent plugin on


syntax on

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Theming
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
colorscheme onedark
let g:airline_theme='onedark'
hi Normal guibg=NONE ctermbg=NONE

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Nerdtree configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let NERDTreeMapOpenInTab='<Space>'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Key Mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let mapleader = " "

map <leader>rr :Ranger<CR>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Sneak configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:sneak#label = 1

let g:mucomplete#enable_auto_at_startup = 1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Omnisharp configuration
"TODO: PopUp preview of doc not working
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:OmniSharp_server_stdio = 1

set completeopt+=menuone,preview,noselect
set completeopt+=preview
set completeopt+=noselect
"set completeopt+=popup
"set completepopup=highlight:Pmenu,border:off
set splitbelow

set shortmess+=c


let g:OmniSharp_highlight_types = 2
let g:omnicomplete_fetch_documentation = 1

augroup omnisharp_commands
    autocmd!

    autocmd FileType cs setlocal omnifunc=OmniSharp#Complete
    autocmd CursorHold *.cs call Omnisharp#TypeLookUpWithoutDocumentation

augroup END
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Syntastic configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_cs_checkers = ['code_checker']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Markdown configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  "Uncomment to override defaults:
  ""let g:instant_markdown_slow = 1
let g:instant_markdown_autostart = 1
let g:instant_markdown_browser = "brave --new-window"
  ""let g:instant_markdown_open_to_the_world = 1
  ""let g:instant_markdown_allow_unsafe_content = 1
  ""let g:instant_markdown_allow_external_content = 0
  ""let g:instant_markdown_mathjax = 1
  ""let g:instant_markdown_logfile =
  ""/tmp/instant_markdown.log'
  ""let g:instant_markdown_autoscroll = 0
  ""let g:instant_markdown_port = 8888
  ""let g:instant_markdown_python = 1

map <C-n> :NERDTreeToggle<CR>
